import React from 'react';
import ReactDOM from 'react-dom';
import App from "./components/App"

document.addEventListener('DOMContentLoaded', function() {
  let parametros, numero;
  parametros = new URLSearchParams(window.location.search);
  
  if (window.location.pathname == '/cgi-bin/koha/opac-detail.pl'){
    $('<div id="id-autores-opac-detail" data-object-id="datos_opac_detail"> </div>').insertAfter($('#ulactioncontainer'));
    numero = parametros.get('biblionumber');
    //numero = 10794;
    const Elemento = document.getElementById('id-autores-opac-detail');
    const objectId = Elemento.getAttribute('data-object-id');
    ReactDOM.render(<App wpObject={window[objectId]} numeroId={numero} />, Elemento);
  }
  
  if (window.location.pathname == '/cgi-bin/koha/opac-authoritiesdetail.pl'){
    $('#userauthdetails').parent().removeClass("order-lg-2").addClass("col-md-6");
    $('<div class="col-md-6" id="id-autores-opac-authoritiesdetail" data-object-id="datos_opac_authoritiesdetail"> </div>').insertAfter($('#userauthdetails').parent());
    
      let registros = $("div.usedin").html().split(": ");
      if(registros.length > 1){
        $("div.usedin > a").html("Aparece en los siguientes registros: "+registros[1].split("<")[0]);
      }
      $("div.authstanza > div.authorized > span.label").html("<b>Use: </b>");
      $("div.authstanzaheading").html("Usado por: ");
      
    numero = parametros.get('authid');
    //numero = 67;
    const Elemento = document.getElementById('id-autores-opac-authoritiesdetail');
    const objectId = Elemento.getAttribute('data-object-id');
    ReactDOM.render(<App wpObject={window[objectId]} numeroId={numero} />, Elemento);
  }
});

