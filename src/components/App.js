import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Fotografia from "./Fotografia"
import "../css/App.scss"
import "../css/Cargando.css"
import google from '../images/google.png';
import ecosur from '../images/ecosur.jpg';
import semantic from '../images/semantic.png';
import research from '../images/research.jpg';
import orcid from '../images/orcid.png';
import publons from '../images/publons.png';
import scopus from '../images/scopus.png';

export default class App extends Component {

  constructor(props) {
    super(props);
    this.state = {
      error: null,
      isLoaded: false,
      items: []
    };
  }
  
  componentDidMount() {
    fetch(this.props.wpObject.url + this.props.numeroId)
      .then(res => res.json())
      .then(
        (data) => {
          this.setState({
            isLoaded: true,
            items: data
          });
        },
        (error) => {
          this.setState({
            isLoaded: true,
            error
          });
        }
      )
  }

  render() {
    const { error, isLoaded, items } = this.state;
    if (error) {
      return <div>Error: {error.message}</div>;
    } else if (!isLoaded) {
      return <Cargando />;
    } else {
    
      var vista = [this.props.wpObject.verNombre,
                   this.props.wpObject.verDatosGenerales,
                   this.props.wpObject.verIdentificadores,
                   this.props.wpObject.verPerfiles];
      if(this.props.wpObject.verNombre == "false"){
        document.querySelectorAll("div.authstanza > div.authorized > span.authorized")[0].innerHTML = items[0][0];
      }
    
      return (
        <div className="container-fluid">
          {items.sort() != "" &&
          <div className="row cuadro-autores-sibe">
            <div className="col-md-12 titulo-autores">
              <h3>{this.props.wpObject.titulo}</h3>
            </div>
            <div className="col-md-12 lista-autores">
              <ul className="list-group lista-au-com">
                {items.map(item => {
                    return <Autor vistaList={vista} info={item} />
                  }
                )}
              </ul>
            </div>
          </div>
          }
        </div>
      );
    }
  }
}

class Autor extends Component {
    render(){
      let numeroRegistros = "https://biblioteca.ecosur.mx/cgi-bin/koha/opac-search.pl?type=opac&q=" + this.props.info[14] + "&idx=an,phr";
      let identificadores = [["ScopusID: ","ORCID: ","Publons: ","CVU: ", "Redalyc: ","Email: "],
                             [this.props.info[3], this.props.info[4], this.props.info[5], this.props.info[6], this.props.info[1], this.props.info[7]],
                             ["https://www.scopus.com/authid/detail.uri?authorId="," https://orcid.org/","https://publons.com/researcher/",
                              "","https://www.redalyc.org/autor.oa?id=","mailto:"],
                             [scopus, orcid, publons, null, null, null]];
      let datosGenerales  = [["Institución: ","Departamento: ","Grupo: ","Unidad: ","Puesto: ", "Área SNI: "],
                             [this.props.info[10], this.props.info[11], this.props.info[12], this.props.info[13], this.props.info[9], this.props.info[15]]];
      let ligaNombre      = "https://biblioteca.ecosur.mx/cgi-bin/koha/opac-authoritiesdetail.pl?authid=" + this.props.info[14];
      return(
        <li className="list-group-item caja-datos-autor">
          <div className="datos-autor-sibe">
            {this.props.vistaList[0]=="false" &&
              <ul>
                <li className="fotografia-autor">
                  <Fotografia correoAutor={identificadores[1][5]} casoNulo="false" />
                </li>
              </ul>
            }
            {this.props.vistaList[0]=="true" &&
              <ul>
              <li className="nombre-autor row">
                  <div className="icono-user-autores">
                    <Fotografia correoAutor={identificadores[1][5]} casoNulo="true" />
                  </div>
                  <a className="liga-nombre" href={ligaNombre}><b>{this.props.info[0]}</b></a>
              </li>
              </ul>
            }
            {this.props.vistaList[1]=="true" &&
              <Datos listaDatos={datosGenerales} />
            }
            {this.props.vistaList[2]=="true" &&
              <Identificadores listaId={identificadores} vistaTitulo={this.props.vistaList[0]} />
            }
            {this.props.vistaList[3]=="true" &&
              <Perfiles listaPerfiles={this.props.info[8]} listaId={identificadores} />
            }
            {this.props.vistaList[0]=="false" &&
              <li className="boton-registros-autor"><a href={numeroRegistros} target="_blank">
                <button type="button" className="btn btn-primary">
                    Ver registros
                </button>
              </a></li>
            }
          </div>
        </li>
      );
    }
}

class Datos extends Component {
    render(){
      let lista = new Array();
      for(let i=0; i<=5; i+=1){
        if(this.props.listaDatos[1][i] != null && this.props.listaDatos[1][i] != ""){
            lista.push([this.props.listaDatos[0][i], this.props.listaDatos[1][i]]);
        }
      }
      return(
        <ul>
          {lista.map(item => (
            <li className="id-autores-cat-item">
              {item[0] == "Unidad: "
              ?
              <b>{item[1]}</b>
              :
              <div><b>{item[0]}</b>{item[1]}</div>
              }
            </li>
          ))}
        </ul>
      );
    }
}

class Identificadores extends Component {
    render(){
      let lista = new Array();
      let idSplit, liga;
      for(let i=0; i<=5; i+=1){
        if(this.props.listaId[1][i] != null && this.props.listaId[1][i] != ""){
            idSplit = this.props.listaId[1][i].split(" ");
            liga    = this.props.listaId[2][i] + idSplit[0];
            lista.push([this.props.listaId[0][i], idSplit[0], liga]);
        }
      }
      if(lista.length > 0){
          return(
            <ul>
                {this.props.vistaTitulo == "false" &&
                  <li className="id-autores-cat"><b>Identificadores</b></li>
                }
                  {lista.map(item => (
                    <li className="id-autores-cat-item">
                      {item[0] != "Email: "
                      ?
                        <div><b>{item[0]}</b>{item[1]}</div>
                      :
                        <div>
                        <b>{item[0]}</b>
                        <a className="liga-identificador" href={item[2]} target="_blank">{item[1]}</a>
                        </div>
                      }
                    </li>
                  ))}
            </ul>
          );
      }else{
        return( <ul></ul> );
      }
    }
}

class Perfiles extends Component {
    render(){
      let listaPer = new Array();
      let ecosurdt = new Array();
      let perfilesSplit = this.props.listaPerfiles.split(" ");
      for(let i=0; i<=perfilesSplit.length-1; i+=1){
        if(perfilesSplit[i].includes("www.ecosur.mx")){
            ecosurdt.push(["ECOSUR", perfilesSplit[i], ecosur]);
        }
        if(perfilesSplit[i].includes("scholar.google")){
            listaPer.push(["Google Scholar", perfilesSplit[i], google]);
        }
        if(perfilesSplit[i].includes("www.researchgate.net")){
            listaPer.push(["ResearcherGate", perfilesSplit[i], research]);
        }
        if(perfilesSplit[i].includes("www.semanticscholar.org")){
            listaPer.push(["Semantic Scholar", perfilesSplit[i], semantic]);
        }
      }
      let listaIden = new Array();
      let idSplit, liga;
      for(let i=0; i<=2; i+=1){
        if(this.props.listaId[1][i] != null && this.props.listaId[1][i] != ""){
            idSplit = this.props.listaId[1][i].split(" ");
            liga    = this.props.listaId[2][i] + idSplit[0];
            let nombreIdenti = this.props.listaId[0][i].split(":")[0];
            if(nombreIdenti === "Orcid"){ nombreIdenti = "ORCID" }
            listaIden.push([nombreIdenti, idSplit[0], liga, this.props.listaId[3][i]]);
        }
      }
      if(listaPer.length > 0 || listaIden.length > 0){
          return(
            <ul>
              <li className="id-autores-cat"><b>Perfiles académicos</b></li>
                  {ecosurdt[0] &&
                    <li className="id-autores-cat-item">
                      <img className="logo-img-perfiles" src={ecosurdt[0][2]} />
                      <a className="liga-identificador" href={ecosurdt[0][1]} target="_blank"> {ecosurdt[0][0]}</a>
                    </li>
                  }
                  {listaIden.map(item => (
                    <li className="id-autores-cat-item">
                        <img className="logo-img-perfiles" src={item[3]} />
                        <a className="liga-identificador" href={item[2]} target="_blank"> {item[0]}</a>
                    </li>
                  ))}
                  {listaPer.map(item => (
                    <li className="id-autores-cat-item">
                      <img className="logo-img-perfiles" src={item[2]} />
                      <a className="liga-identificador" href={item[1]} target="_blank"> {item[0]}</a>
                    </li>
                  ))}
            </ul>
          );
      }else{
        return( <ul></ul> );
      }
    }
}

class Cargando extends Component {
    render(){
        return(
            <div className="spinner-datos-autores">
              <div className="rect1"></div>
              <div className="rect2"></div>
              <div className="rect3"></div>
              <div className="rect4"></div>
              <div className="rect5"></div>
            </div>
        );
    }
}

App.propTypes = {
  wpObject: PropTypes.object
};

