import React, { Component } from 'react';
import PropTypes from 'prop-types';

export default class Fotografia extends Component {

  constructor(props) {
    super(props);
    this.state = {
      error: null,
      isLoaded: false,
      image: []
    };
  }
  
  componentDidMount() {
    var myHeaders = new Headers();
    myHeaders.append("Content-Type", "application/json");
    var graphql = JSON.stringify({
      query: 'query{\ngetPersonaPorEmail(email:"'+this.props.correoAutor+'"){\nhashtag\nfotografia\ngrupo\ntaggrupo\ndepartamento\ntagdepto\nunidad\nidunidad\n}\n}'
    });

    var requestOptions = {
      method: 'POST',
      headers: myHeaders,
      body: graphql,
      redirect: 'follow'
    };
    
    fetch("https://graphql.web.ecosur.mx/graphql", requestOptions)
      .then(res => res.json())
      .then(
        (data) => {
          this.setState({
            isLoaded: true,
            image: data
          });
        },
        (error) => {
          this.setState({
            isLoaded: true,
            error
          });
        }
      )
  }

  render() {
    const { error, isLoaded, image } = this.state;
    if (error) {
      return <div>Error: {error.message}</div>;
    } else if (!isLoaded) {
      return <div>  </div>;
    } else {
        var condicional = false;
        if(image.data.getPersonaPorEmail !== null && image.data.getPersonaPorEmail.fotografia !== null){
            var imageSRC="https://sii.ecosur.mx/Content/personal/fotos/"+image.data.getPersonaPorEmail.fotografia+".jpg"
            condicional = true;
        }
        
      return (
        <div>
            {condicional==true
            ?
            <img className="foto-autor" src={imageSRC} />
            :
            this.props.casoNulo=="true" && <i className="fa fa-user fa-2x" aria-hidden="true"></i>
            }
        </div>
      );
    }
  }
}

