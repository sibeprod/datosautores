# Visibilidad de datos de autores en registro bibliografico y en detalles de autoridad

![Screenshot](src/images/screenshot.png)

***Demos:***
- Autores de un registro bibliografico - https://biblioteca.ecosur.mx/cgi-bin/koha/opac-detail.pl?biblionumber=10794
- Detalles de autoridad - https://biblioteca.ecosur.mx/cgi-bin/koha/opac-authoritiesdetail.pl?authid=67

## Requerimientos
---
1. Generar un informe SQL en Koha:

	En primer lugar, debe crear uno o más informes públicos en los que se basara el componente.

	SQL para obtener los datos de identificadores de los autores que se encuentran catalogados de un registro bibliográfico a través de un `biblionumber`:
	```sql
	SELECT 
        ExtractValue(marcxml,'//datafield[@tag="100"]/subfield[@code="a"]') as nombre,
        ExtractValue(marcxml,'//datafield[@tag="024" and subfield[@code="2"]="redalyc" ]/subfield[@code="a"]') as redalyc,
        ExtractValue(marcxml,'//datafield[@tag="024" and subfield[@code="2"]="loop" ]/subfield[@code="a"]') as id_loop,
        ExtractValue(marcxml,'//datafield[@tag="024" and subfield[@code="2"]="scopus" ]/subfield[@code="a"]') as scopus,
        ExtractValue(marcxml,'//datafield[@tag="024" and subfield[@code="2"]="orcid" ]/subfield[@code="a"]') as orcid,
        ExtractValue(marcxml,'//datafield[@tag="024" and subfield[@code="2"]="rid" ]/subfield[@code="a"]') as publons,
        ExtractValue(marcxml,'//datafield[@tag="024" and subfield[@code="2"]="cvu" ]/subfield[@code="a"]') as cvu,
        ExtractValue(marcxml,'//datafield[@tag="371"]/subfield[@code="m"]') as email,
        ExtractValue(marcxml,'//datafield[@tag="371"]/subfield[@code="6"]') as perfiles_academicos,
        ExtractValue(marcxml,'//datafield[@tag="372"]/subfield[@code="a"]') as puesto,
        ExtractValue(marcxml,'//datafield[@tag="373"]/subfield[@code="a"]') as institucion,
        ExtractValue(marcxml,'//datafield[@tag="373"]/subfield[@code="u"]') as departamento,
        ExtractValue(marcxml,'//datafield[@tag="373"]/subfield[@code="v"]') as grupo,
        ExtractValue(marcxml,'//datafield[@tag="373"]/subfield[@code="0"]') as unidad,
        authid as id_autoridad
    FROM auth_header ah
    WHERE 
        authtypecode='PERSO_NAME'  
        AND FIND_IN_SET( ah.authid, (
                                        SELECT  
                                            REPLACE (
                                                CONCAT_WS('',
                                                    ExtractValue(metadata,'//datafield[@tag="100"]/subfield[@code="9"]'),
                                                    ExtractValue(metadata,'//datafield[@tag="700"]/subfield[@code="9"]')
                                                ) , ' ', ','    
                                            ) AS 'autoridad_id'
                                        FROM 
                                            biblio_metadata bm 
                                        WHERE 
                                            bm.biblionumber =  <<biblionumber>> 
                                    )
        )
	``` 
    Esto generara un url API-SVC  
        https://biblioteca.ecosur.mx/cgi-bin/koha/svc/report?id=25&param_name=biblionumber&sql_params={biblionumber}


    SQL para obtener información de un solo autor (identificadores, perfiles académicos, datos generales, institución, puesto, etc.) a través de un `authid`
    ```sql
	SELECT 
        ExtractValue(marcxml,'//datafield[@tag="100"]/subfield[@code="a"]') as nombre,
        ExtractValue(marcxml,'//datafield[@tag="024" and subfield[@code="2"]="redalyc" ]/subfield[@code="a"]') as redalyc,
        ExtractValue(marcxml,'//datafield[@tag="024" and subfield[@code="2"]="loop" ]/subfield[@code="a"]') as id_loop,
        ExtractValue(marcxml,'//datafield[@tag="024" and subfield[@code="2"]="scopus" ]/subfield[@code="a"]') as scopus,
        ExtractValue(marcxml,'//datafield[@tag="024" and subfield[@code="2"]="orcid" ]/subfield[@code="a"]') as orcid,
        ExtractValue(marcxml,'//datafield[@tag="024" and subfield[@code="2"]="rid" ]/subfield[@code="a"]') as publons,
        ExtractValue(marcxml,'//datafield[@tag="024" and subfield[@code="2"]="cvu" ]/subfield[@code="a"]') as cvu,
        ExtractValue(marcxml,'//datafield[@tag="371"]/subfield[@code="m"]') as email,
        ExtractValue(marcxml,'//datafield[@tag="371"]/subfield[@code="6"]') as perfiles_academicos,
        ExtractValue(marcxml,'//datafield[@tag="372"]/subfield[@code="a"]') as puesto,
        ExtractValue(marcxml,'//datafield[@tag="373"]/subfield[@code="a"]') as institucion,
        ExtractValue(marcxml,'//datafield[@tag="373"]/subfield[@code="u"]') as departamento,
        ExtractValue(marcxml,'//datafield[@tag="373"]/subfield[@code="v"]') as grupo,
        ExtractValue(marcxml,'//datafield[@tag="373"]/subfield[@code="0"]') as unidad,
        authid as id_autoridad
    FROM auth_header ah
    WHERE 
        authtypecode='PERSO_NAME'  
        AND FIND_IN_SET( ah.authid,  <<autores_id>> )
	``` 
    Esto generara un url API-SVC  
        https://biblioteca.ecosur.mx/cgi-bin/koha/svc/report?id=26&param_name=autores_id&sql_params={id-del-autor}


## Uso
--- 

1. El script compilado como el css deberan estar cargados previamente en Wrodpress
2. Agregar en la sección de Noticias Koha el siguiente código:

```html
<!-- opaccredits -->
<script type="text/javascript" id="autores-sibe">
    var datos_opac_detail = {
        "url":"https:\/\/biblioteca.ecosur.mx\/cgi-bin\/koha\/svc\/report?id=25&param_name=biblionumber&sql_params=",
        "titulo":"Identificadores de autores",
        "verNombre":"true",
        "verDatosGenerales":"false",
        "verIdentificadores":"true",
        "verPerfiles":"false"
    };
    var datos_opac_authoritiesdetail = {
        "url":"https:\/\/biblioteca.ecosur.mx\/cgi-bin\/koha\/svc\/report?id=26&param_name=autores_id&sql_params=",
        "titulo":"Detalles del autor",
        "verNombre":"false",
        "verDatosGenerales":"true",
        "verIdentificadores":"true",
        "verPerfiles":"true"
    };
</script>
<script type="text/javascript" src="https://sibeservicios.ecosur.mx/sibe/wp-content/themes/education-base-child/wp-includes/datosautores/dist/index.js"></script>
<link href="https://sibeservicios.ecosur.mx/sibe/wp-content/themes/education-base-child/wp-includes/datosautores/dist/css/index.css" rel="stylesheet" />
```

## Instalación y modificaciones en dev
```bash
# Instalar dependencias del componente 
npm install

#compilar componente
npm run build
```

## Creditos
- Sistema de Información Bibliotecario de ECOSUR
- [Germán de Jesús Hernández García](https://gitlab.com/gjhernandez1234)
- [Saul Oswaldo Lara Gonzalez](https://gitlab.com/solg713)
